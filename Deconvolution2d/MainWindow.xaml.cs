﻿using Microsoft.Win32;
using NoizeFilter;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Deconvolution2d
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BitmapImage bmp;
        private WriteableBitmap wbm, wbm_filt;
        private YBytes[,] yb, new_yb;
        private int w, h, stride;

        /// <summary>
        /// Stride for WriteableBitmap.
        /// </summary>
        public int Stride => stride;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Bttn_Browse_Click(object sender, RoutedEventArgs e)
        {
            OpenFileDialog dlg = new OpenFileDialog
            {
                DefaultExt = ".bmp",
                Filter = "BMP Files (.bmp)|*.bmp"
            };

            if (dlg.ShowDialog() ?? false)
            {
                try
                {
                    using (FileStream fs = new FileStream(dlg.FileName, FileMode.Open))
                    {
                        bmp = new BitmapImage(new Uri(dlg.FileName, UriKind.Relative));
                        Tb_Path.Text = dlg.FileName;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, ex.Message, "Ошибка открытия файла");
                }

                yb = new YBytes[(int)bmp.PixelWidth, (int)bmp.PixelHeight];
                stride = bmp.PixelWidth * bmp.Format.BitsPerPixel / 8;

                for (int i = 0; i < bmp.PixelWidth; i++)
                {
                    for (int j = 0; j < bmp.PixelHeight; j++)
                    {
                        byte[] p = new byte[bmp.PixelHeight];// !!!!!!!
                        bmp.CopyPixels(new Int32Rect(i, j, 1, 1), p, stride, 0);

                        yb[i, j] = new YBytes(p, true);

                        // и новый битмэп на всякий пожарный
                    }
                }

                To2P(bmp.PixelWidth, bmp.PixelHeight, out w, out h);
                new_yb = new YBytes[w, h];

                Interpolation(bmp, w, h, yb, ref new_yb);

                wbm = new WriteableBitmap(w, h, bmp.DpiX, bmp.DpiY,
                    PixelFormats.Bgra32, null);

                wbm_filt = wbm.Clone();

                for (int i = 0; i < w; i++)
                {
                    for (int j = 0; j < h; j++)
                    {
                        wbm.WritePixels(new Int32Rect(i, j, 1, 1),
                            new_yb[i, j].ToARGB(), stride, 0);
                    }
                }
                
                Bttn_Start.IsEnabled = true;
            }
        }

        private void Bttn_Start_Click(object sender, RoutedEventArgs e)
        {
            var sigX = double.Parse(Tb_sigmaX.Text);
            var sigY = double.Parse(Tb_sigmaY.Text);

            double? A = null;
            if (IsNull(Tb_Amp.Text) != null) A = double.Parse(Tb_Amp.Text);

            var filt = GetFilt(w, h, sigX, sigY, A);

            var Eps = double.Parse(Tb_Eps.Text);

            var win = new WindowDeconvolution
            {
                W = w,
                H = h,
                MW = this,
                Owner = this,
                Bytes = new_yb,
                MainBMP = wbm,
                Filter = filt,
                BMPWithFilter = wbm_filt,
                Eps = Eps,
                L1 = (int)(3.0 * sigX),
                L2 = (int)(3.0 * sigY),
            };

            win.Start();

            win.ShowDialog();
        }

        public YBytes[,] GetFilt(int w, int h, double sigX, double sigY, double? A)
        {
            int x0 = w / 2;
            int y0 = h / 2;
            
            var yb = new YBytes[w, h];

            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    yb[i, j] = new YBytes(F(i, j, x0, y0, sigX, sigY, A));
                }
            }

            return yb;
        }

        private byte F(int x, int y, int x0, int y0,
            double sig_x, double sig_y, double? A)
        {
            int dx = x - x0;
            int dy = y - y0;
            double a = A ?? 254.9;

            double res = a * Math.Exp((-dx * dx / sig_x / sig_x)
                - (-dy * dy / sig_y / sig_y));

            if (res > 255.0) return 255;
            if (res < 0.0) return 0;

            return (byte)res;
        }

        void Interpolation(BitmapImage IBmp, int width, int height,
            YBytes[,] colors, ref YBytes[,] newcolors)
        {
            double[,] dm = new double[width, height];

            double x_r = (double)IBmp.PixelWidth / (double)width;
            double y_r = (double)IBmp.PixelHeight / (double)height;

            for (int i = 0; i < height - 1; i++)
            {
                for (int j = 0; j < width - 1; j++)
                {
                    int x = (int)(x_r * j);
                    int y = (int)(y_r * i);
                    double dx = (x_r * j) - x;
                    double dy = (y_r * i) - y;

                    int A = colors[x, y].Y;
                    int B = colors[x + 1, y].Y;
                    int C = colors[x, y + 1].Y;
                    int D = colors[x + 1, y + 1].Y;

                    dm[j, i] = A + (B - A) * dx + (C - A) * dy + (D + A - B - C) * dx * dy;

                    if (dm[j, i] > 255.0) dm[j, i] = 255.0;
                    else if (dm[j, i] < 0.0) dm[j, i] = 0.0;
                }
            }

            for (int i = 0; i < width; i++)
            {
                for (int j = 0; j < height; j++)
                {
                    newcolors[i, j] = new YBytes((byte)dm[i, j]);
                }
            }
        }

        private static void To2P(double width, double height,
            out int newwidth, out int newheight)
        {
            newheight = getP(height);
            newwidth = getP(width);
        }

        private static int getP(double i)
        {
            int ch = 2;
            while (ch < (int)i) ch *= 2;
            return ch;
        }

        private static string IsNull(string s)
        {
            return s == string.Empty || s == "-" ?
                null : s;
        }
    }
}
