﻿using NoizeFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Deconvolution2d
{
    /// <summary>
    /// Логика взаимодействия для WindowDeconvolution.xaml
    /// </summary>
    public partial class WindowDeconvolution : Window
    {
        public int W { set; get; }
        public int H { set; get; }
        public double Eps { set; get; }
        public int L1 { set; get; }
        public int L2 { set; get; }

        public MainWindow MW { set; get; }

        /// <summary>
        /// исходное изображение
        /// </summary>
        public YBytes[,] Bytes { set; get; }

        /// <summary>
        /// Фильтр.
        /// </summary>
        public YBytes[,] Filter { set; get; }
        
        /// <summary>
        /// битмэп исходного изображения
        /// </summary>
        public WriteableBitmap MainBMP { set; get; }

        /// <summary>
        /// 
        /// </summary>
        public WriteableBitmap BMPWithFilter { set; get; }

        public WindowDeconvolution()
        {
            InitializeComponent();
        }

        public void Start()
        {
            Image_Original.Source = MainBMP;

            YBytes[,] ybf = Convolution(W, H, Bytes, Filter, L1, L2);

            for (int i = 0; i < W; i++)
            {
                for (int j = 0; j < H; j++)
                {
                    BMPWithFilter.WritePixels(new Int32Rect(i, j, 1, 1),
                        ybf[i, j].ToARGB(), MW.Stride, 0);
                }
            }

            Image_With_Filter.Source = BMPWithFilter;
        }

        /// <summary>
        /// Двумерная свёртка.
        /// </summary>
        /// <param name="w">Width.</param>
        /// <param name="h">Height.</param>
        /// <param name="X">Исходный сигнал.</param>
        /// <param name="H">Импульсная характеристика.</param>
        /// <returns>Возвращает результат свёртки.</returns>
        public YBytes[,] Convolution(int w, int h, YBytes[,] X, YBytes[,] H, int l1, int l2)
        {
            double max = 0;

            var long_Y = new long[w, h];
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    long sum = 0;

                    for (int ii = 0; ii < l1; ii++)
                    {
                        for (int jj = 0; jj < l2; jj++)
                        {
                            int div_i = i - ii;
                            int div_j = j - jj;

                            sum += H[ii, jj] *
                                X[div_i < 0 ? w + div_i : div_i, div_j < 0 ? h + div_j : div_j];
                        }
                    }

                    long_Y[i, j] = sum / w / h;

                    if (long_Y[i, j] > max) max = long_Y[i, j];
                }
            }

            var Y = new YBytes[w, h];
            for (int i = 0; i < w; i++)
            {
                for (int j = 0; j < h; j++)
                {
                    double a = (double)long_Y[i, j] / max * 255.0;
                    if (a < 0.0) a = 0.0;

                    Y[i, j] = (byte)a;
                }
            }

            return Y;
        }
    }
}
