﻿using System;

namespace NoizeFilter.Data
{
    /// <summary>
    /// Класс святого Фурье.
    /// </summary>
    public static class Fourier
    {

        /// <summary>
        /// Считает прямое преобразование Фурье
        /// </summary>
        /// <param name="input">Входные данные</param>
        /// <returns>Возвращает спектр</returns>
        static public Complex[] FFT(Complex[] input)
        {
            Complex[] res = SwapElements(input);

            FFTBody(res, false);

            return res;
        }

        /// <summary>
        /// Считает прямое преобразование Фурье
        /// </summary>
        /// <param name="input">Входные данные</param>
        /// <returns>Возвращает спектр</returns>
        static public Complex[] FFT(byte[] input)
        {
            Complex[] res = SwapElements(input);

            FFTBody(res, false);

            return res;
        }

        /// <summary>
        /// Считает обратное преобразование Фурье
        /// </summary>
        /// <param name="input">Входные данные</param>
        /// <returns>Возвращает спектр</returns>
        static public Complex[] IFFT(Complex[] input)
        {
            Complex[] res = SwapElements(input);

            FFTBody(res, true);

            return res;
        }

        private static void FFTBody(Complex[] res, bool inverse)
        {
            int Size = res.Length;

            // Двоичный логарифм размера
            int lg;
            for (lg = 0; Size >> lg > 1; lg++) ;

            for (Int32 s = 1; s <= lg; ++s)
            {
                int m = 1 << s;
                Complex Wm = inverse ?
                    new Complex(Math.Cos(2.0 * Math.PI / m), Math.Sin(2.0 * Math.PI / m)) :
                    new Complex(Math.Cos(2.0 * Math.PI / m), Math.Sin(-2.0 * Math.PI / m));

                Complex W = 1.0;

                Complex t = new Complex();
                //Complex u;

                for (int j = 0; j < m / 2; ++j)
                {
                    for (int k = j; k < Size; k += m)
                    {
                        t = new Complex(W.Re * res[k + m / 2].Re - W.Im * res[k + m / 2].Im,
                                        W.Re * res[k + m / 2].Im + W.Im * res[k + m / 2].Re);
                        Complex u = new Complex(res[k].Re, res[k].Im);

                        res[k].Re = u.Re + t.Re;
                        res[k].Im = u.Im + t.Im;

                        /*res[k + m / 2].Re = u.Re - t.Re;
						res[k + m / 2].Im = u.Im - t.Im;*/
                        res[k + m / 2] = u - t;

                    }

                    t.Re = W.Re * Wm.Re - W.Im * Wm.Im;
                    t.Im = W.Re * Wm.Im + W.Im * Wm.Re;

                    W = t;
                }
            }

            if (inverse)
            {
                int count = res.Length;
                for (int i = 0; i < count; ++i)
                {
                    res[i] /= count;
                }
            }
        }


        private static Complex[] SwapElements(Complex[] input)
        {
            int Size = input.Length;

            // Двоичный логарифм размера
            Int32 Lg;
            for (Lg = 0; Size >> Lg > 1; Lg++) ;

            Complex[] res = new Complex[Size];

            for (Int32 i = 0; i < Size; ++i)
            {
                int k = 0;
                for (int j = 0; j < Lg; ++j)
                {
                    if ((i & (1 << j)) == (1 << j))
                    {
                        k += 1 << (Lg - j - 1);
                    }
                }

                /*res[i].Re = input[k].Re;
				res[i].Im = input[k].Im;*/

                res[i] = input[k];
            }

            return res;
        }


        private static Complex[] SwapElements(byte[] input)
        {
            int Size = input.Length;

            // Двоичный логарифм размера
            Int32 Lg;
            for (Lg = 0; Size >> Lg > 1; Lg++) ;

            Complex[] res = new Complex[Size];

            for (int i = 0; i < Size; ++i)
            {
                int k = 0;
                for (Int32 j = 0; j < Lg; ++j)
                {
                    if ((i & (1 << j)) == (1 << j))
                    {
                        k += 1 << (Lg - j - 1);
                    }
                }

                /*res[i].Re = input[k].Re;
				res[i].Im = input[k].Im;*/

                res[i] = (double)input[k];
            }

            return res;
        }

    }
}
